FROM phusion/baseimage

LABEL maintainer="Jonathan Vagnier <adm.jvagnier@gmail.com>"

# Set the labels that are used for OpenShift to describe the builder image.
LABEL io.k8s.description="Nginx on Ubuntu image" \
    io.k8s.display-name="NGinx Ubuntu" \
    io.openshift.expose-services="8080:http" \
    io.openshift.tags="builder,webserver,html,proxy,ubuntu,nginx" \
    # this label tells s2i where to find its mandatory scripts
    # (run, assemble, save-artifacts)
    io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

RUN INSTALL_PKGS="nginx" && \
    # Packages installation
    apt-get update && \
    apt-get install \
        -y --force-yes \
        --no-install-suggests \
        --no-install-recommends \
        ${INSTALL_PKGS} && \
    # Installation cleanup
    apt-get autoclean && \
    apt-get clean && \
    rm -rf \
        /var/lib/apt/lists/* \
        /var/cache/apt/archives/*

# Change the default port for nginx 
# Required if you plan on running images as a non-root user).
RUN sed -i 's/80/8080/' /etc/nginx/sites-available/default
RUN sed -i 's/user www-data;//' /etc/nginx/nginx.conf

COPY .s2i/bin /usr/libexec/s2i
RUN find /usr/libexec/s2i -type f -print0 | xargs chmod +x

RUN chown -R 1001:1001 /usr/share/nginx
RUN chown -R 1001:1001 /var/log/nginx
RUN chown -R 1001:1001 /var/lib/nginx
RUN touch /run/nginx.pid
RUN chown -R 1001:1001 /run/nginx.pid
RUN chown -R 1001:1001 /etc/nginx
RUN chown -R 1001:1001 /var/www

USER 1001

# Set the default port for applications built using this image
EXPOSE 8080

# Modify the usage script in your application dir to inform the user how to run
# this image.
CMD [ "/usr/libexec/s2i/usage" ]